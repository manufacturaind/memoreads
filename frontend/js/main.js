var vm = new Vue({
  el: '#app',
  name: 'app',
  delimiters: ['${', '}'],
  data: {
    notes: [],
    activeNotes: [],
    activeButton: null,
    days: [1,2,3,4,5,6],
    contents: null,
    loading: true,
  },
  mounted: function() {
    this.getAllNotes();
    var urlParams = new URLSearchParams(window.location.search);
    var snapshotStr = urlParams.get('snapshot');
    if (snapshotStr) {
      for (name of snapshotStr.split('|')) {
        this.loadNote(name)
      }
    }
    var clipboard = new ClipboardJS('#snapshot');
    console.log(clipboard)
    clipboard.on('success', function(e) {
      console.log('copy')
      document.querySelector('#snapshot').innerHTML = "Snapshot link copied to clipboard!"
    });
  },
  computed: {
    activeNoteNames: function() {
      return vm.activeNotes.map(x => x.name)
    },
    snapshotString: function() {
      return this.activeNotes.map(x => x.name).join('|')
    },
  },
  methods: {
    getAllNotes: function() {
      axios.get('/api/notes/')
        .then(function(response) {
          console.log(response.data)
          vm.notes = response.data.notes;
          console.log(vm.notes)
        })
        .catch(function(error) {
          console.log(error);
          vm.errored = true;
        })
        .finally(function() {
          vm.loading = false;
          for (button of document.querySelectorAll('.note-button')) {
            button.onclick = function () {
              if (vm.activeNoteNames.includes(this.id)) {
                vm.closeNote(this.id)
                console.log(vm.activeNoteNames)
              } else {
                vm.loadNote(this.id)
                console.log(vm.activeNoteNames)
              }
            }
            // change here to set the button style according to content size
            button.style = 'width: ' + button.dataset.size * 8 + 'px;';
          }
        });
    },

    closeNote: function(name) {
      var note = vm.activeNotes.filter(x => x.name == name)[0]
      var idx = vm.activeNotes.indexOf(note)
      vm.activeNotes.splice(idx, 1)
    },

    loadNote: function(name) {
      axios.get('/api/note/', {params: {name: name}})
        .then(function(response) {
          // add new note to beginning of array
          vm.activeNotes.unshift(response.data)
        })
        .catch(function(error) {
          console.log(error);
          vm.errored = true;
        })
        .finally(function() {
          vm.loading = false;
          for (link of document.querySelectorAll('.wikilink')) {
            link.onclick = function (ev) {
              ev.preventDefault()
              vm.loadNote(link.innerText)
            }
          }
          for (span of document.querySelectorAll('.hash')) {
            span.style = 'color: #' + span.innerHTML + ';';
            span.innerHTML = '●'
          }
          console.log('Added: ' + name)
        });
    },

    loadRandomNote: function() {
      var note = vm.notes[Math.floor(Math.random() * vm.notes.length)]
      vm.loadNote(note.name)
    },
  }
});
